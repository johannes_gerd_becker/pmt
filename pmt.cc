// Trying to implement a "poor man's tuple" (pmt), which holds the tuple elements in a lambda capture.
// The basic idea is the observation that, formally, a
//   tuple t = ( x1, x2, ..., xn )
// can be seen as a functional t' with t'(f) := f (x1, ... xn ).
//
// So, actually, the pmt's are functionals.
//
// On VC++ 15 affected by this issue:
//    http://stackoverflow.com/questions/40950203/vc-15-calls-the-wrong-copy-constructor-for-lambda-capture

#include <cassert>
#include <functional>
#include <iostream>
#include <memory>
#include <type_traits>


namespace poor {
  // idea: http://codereview.stackexchange.com/a/67572/124791
  template <std::size_t DesiredIndex, class = std::make_index_sequence<DesiredIndex>>
  struct GetNthParam;

  template <std::size_t DesiredIndex, std::size_t ... ignore_n>
  struct GetNthParam<DesiredIndex, std::index_sequence<ignore_n ...>> {
    
    
    struct Ignore { template <class ... Args> Ignore(Args && ...) {}; };

    template <class Arg, class ... Args>  
    constexpr decltype(auto) operator () (decltype(ignore_n, Ignore{}) ..., Arg && arg, Args && ...) {
      return std::forward<Arg> (arg);
    }
  };


  namespace detail {
    
    // idea: http://stackoverflow.com/a/24837449/1419315
    template<class T>
    struct auto_mover {
      explicit auto_mover(T const & t) : t (t) {};
      explicit auto_mover(T && t) : t (std::move(t)) {};
      auto_mover (auto_mover & other) : t (std::move(other.t)) {};
      auto_mover (auto_mover const &) = default;
      auto_mover (auto_mover &&) = default;
      auto_mover& operator = (auto_mover const &) = default;
      auto_mover& operator = (auto_mover &&) = default;
      
      mutable T t;
    };

    struct Wrap {
      template <class T> auto_mover<T> operator () (T && t) const { return auto_mover<T>{std::forward<T>(t)}; }
      template <class T> std::reference_wrapper<T> operator () (T & t) const { return {t}; }
    };

    struct MutableUnwrap {
      template <class T> T & operator () (auto_mover<T> const & am) const { return am.t; }
      template <class T> T & operator () (std::reference_wrapper<T> const & r) const { return r.get(); }
    };

    struct ConstUnwrap {
      template <class T> T const & operator () (auto_mover<T> const & am) const { return am.t; }
      template <class T> T & operator () (std::reference_wrapper<T> const & r) const { return r.get(); }
    };

    struct MoveUnwrap {
      template <class T> T && operator () (auto_mover<T> const & am) const { return static_cast<T &&> (am.t); }
      template <class T> T & operator () (std::reference_wrapper<T> const & r) const { return r.get(); }
    };

  }

  template <class ... Ts>
  struct pmt {

    template <class ... UTypes>
    pmt(UTypes && ... ts) : holder(create_holder(std::forward<UTypes> (ts) ...)) {};

    pmt (pmt const &) = default;
    pmt (pmt &&) = default; 
    pmt & operator = (pmt const & other) = default;
    pmt & operator = (pmt &&) = default;
    
    template <class F>
    decltype(auto) operator () (F f) & {
      return holder(detail::MutableUnwrap{}, f);
    }
    
    template <class F>
    decltype(auto) operator () (F f) const & {
      return holder(detail::ConstUnwrap{}, f);
    }

    template <class F>
    decltype(auto) operator () (F f) && {
      return holder(detail::MoveUnwrap{}, f);
    }
    
    
  private:
    static auto create_holder (Ts ... ts) {
        return ([](auto ... p){
          return [=] (auto unwrap, auto f) -> decltype(auto) {
              return f(unwrap(p)...);
          };
      })(detail::Wrap {} (std::forward<Ts>(ts)) ...);
    }
    
    using holder_type = decltype (create_holder (std::declval<Ts> () ...));
    
    // Caution: This is very fragile. If this is treated as a non-const lvalue reference,
    //   it permits its values to be moved from. So never treat this as a
    //   non-const lvalue reference.
    holder_type holder;
  };

  template <std::size_t N, class ... Ts>
  auto & get (pmt<Ts ...> & tpl) {
      return tpl (GetNthParam<N> {});
  }

  template <std::size_t N, class ... Ts>
  auto const & get (pmt<Ts ...> const & tpl) {
      return tpl (GetNthParam<N> {});
  }

  template <std::size_t N, class ... Ts>
  decltype(auto) get (pmt<Ts ...> && tpl) {
    return std::move(tpl) (GetNthParam<N> {});
  }


  template <class ... Args>
  auto make_pmt (Args && ... args) {
      return pmt<Args ...> (std::forward<Args> (args) ...);
  }
}

int main() {
    using poor::get;
    using poor::pmt;
    using poor::make_pmt;
    
    { // values
      pmt<int,int,int> t(100,110,120);
      assert(get<0> (t) == 100);
      assert(get<1> (t) == 110);
      assert(get<2> (t) == 120);

      get<0> (t) = 600;

      assert(get<0> (t) == 600);
    }
    
    { // references
      int x = 0, y = 0, z = 0;
      pmt<int&,int&,int&> s(x,y,z);
      assert(get<0> (s) == 0);
      assert(get<1> (s) == 0);
      assert(get<2> (s) == 0);

      x=5;

      assert(get<0> (s) == 5);
    }
    
    { // move-only types
        auto tpl = make_pmt (std::make_unique<int> (4), std::make_unique<int> (2));
        assert ( * get<0> (tpl) == 4);
        assert ( * get<1> (tpl) == 2);
    }
    
    { // moving tuples with move-only types
      auto tpl = make_pmt (std::make_unique<int> (4), std::make_unique<int> (2));
      auto tpl2 = std::move(tpl);
      assert(!(get<0> (tpl)));
      assert(!(get<1> (tpl)));
      assert(!!(get<0> (tpl2)));
      assert(!!(get<1> (tpl2)));
      assert( * get<0> (tpl2) == 4 );
      assert( * get<1> (tpl2) == 2 );      
    }

    { // moving from an rvalue tuple
      auto tpl = make_pmt (std::make_unique<int> (4), std::make_unique<int> (2));
      auto p = get<0> (std::move(tpl));
      assert (!!p);
      assert ( * p == 4);
      assert ( ! get<0> (tpl) );
    }
    
    return 0;
}